#include <string>
#include <random>
#include <iostream>
using namespace std;

using std::string;

string randDNA(int seed, string bases, int n)
{
int num;
int max;
string seq="";
max=bases.size()-1;
random_device rd;
mt19937 eng1(seed);
uniform_int_distribution<> uni(0, max);

for ( int i =0; i<n; i++)
{
num = uni(eng1);
seq=seq+bases[num];
}


return seq;

}
